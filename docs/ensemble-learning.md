# 앙상블 학습: Bagging, Boosting과 Stacking

## <a nmae="intro"></a> 개요
앙상블 학습(ensemble learning)은 기계 학습 모델의 성능과 정확성을 향상시키기 위한 강력한 기술이다. 각각 데이터의 다른 부분 집합 또는 변형에 대해 훈련된 여러 모델을 결합하여 어떤 단일 모델보다 강력하고 신뢰할 수 있는 최종 예측을 생성하는 것을 포함한다. 앙상블 학습은 과적합, 과소적합, 편향 및 분산과 같은 개별 모델의 한계를 극복하는 데 도움이 될 수 있다.

이 포스팅에서는 Python에서 인기 있는 세 가지 앙상블 학습 방법인 bagging, boosting과 stacking을 사용하는 방법을 설명한다. 또한 실제 데이세트에서 서로 다른 앙상블 방법의 성능을 비교하고 평가하는 방법도 설명할 것이다. 이 포스팅에 대한 학습을 통하여 다음을 수행할 수 있을 것이다.

- 앙상블 학습의 개념과 이점
- scikit-learn을 사용하여 bagging, boosting 및 stacking 구현
- 분류 문제에 앙상블 학습 방법 적용
- 결과 분석과 다양한 앙상블 방법의 성능 비교

시작하기 전에 Python과 데이터 분석에 대한 기본 지식이 필요하다. 또한 다음과 같은 라이브러리를 설치해야 한다.

```python
# Import the libraries
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.model_selection import train_test_split, cross_val_score
from sklearn.metrics import accuracy_score, confusion_matrix, classification_report
from sklearn.ensemble import BaggingClassifier, AdaBoostClassifier, GradientBoostingClassifier, StackingClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
```

Python에서 앙상블 학습 방법을 사용하는 법을 배울 준비가 되었나요? 시작해 봅시다!

## <a nmae="sec_02"></a> 앙상블 러닝이란?
앙상블 학습은 여러 모델을 결합하여 어떤 단일 모델보다 더 정확하고 강력한 최종 예측을 산출하는 기계 학습 기법이다. 앙상블 학습의 아이디어는 전문가 팀이 개별 전문가보다 더 나은 결정을 내릴 수 있는 것처럼 약한 학습자 그룹이 함께 협력하여 강력한 학습자를 형성할 수 있다는 것이다.

모델들의 앙상블을 만드는 방법은 모델들이 어떻게 훈련되고 어떻게 결합되는가에 따라 다양하다. 일반적으로 앙상블 학습 방법에는 크게 세 가지 타입이 있다

- **Bagging**: Bagging은 부트스트랩 집계(bootstrap aggregation)를 의미한다. 이것은 여러 모델을 각각 데이터의 다른 임의의 부분집합에 대해 훈련시킨 다음 예측의 평균을 내는 것을 포함한다. Bagging은 모델의 분산을 줄이고 모델의 일반화 능력을 향상시킬 수 있다.
- **Boosting**: boosting은 여러 모델을 수정된 버전의 데이터에 대해 순차적으로 훈련시킨 다음 예측값을 성능에 따라 가중치를 부여하는 것이다. boosting은 모델의 편향과 분산을 줄이고 정확도를 향상시킬 수 있다.
- **Stacking**: stacking은 동일하거나 다른 데이터에 대해 여러 모델을 훈련시킨 다음 다른 모델을 사용하여 예측을 결합하는 것을 포함한다. stacking은 다양한 모델의 장점을 활용하고 다양성을 향상시킬 수 있다.

이 포스팅에서는 이러한 앙상블 학습 방법을 각각 기계 학습 인기 라이브러리인 scikit-learn을 활용하여 Python으로 구현하는 방법을 보인다. 또한 실제 분류 문제에 적용하는 방법과 성능을 비교하는 방법도 배우게 된다.

## <a nmae="sec_03"></a> Bagging: 부트스트랩 집계
bagging은 가장 간단하고 널리 사용되는 앙상블 학습 방법 중 하나이다. 부트스트랩 집계(bootstrap aggregating)의 약자로, 데이터의 서로 다른 부트스트랩 샘플에 대해 학습된 여러 모델의 예측을 결합하는 것을 의미한다. 부트스트랩 샘플은 데이터를 대체하여 무작위로 추출한 샘플로, 일부 데이터 포인트는 두 번 이상 나타날 수 있고 일부 데이터 포인트는 전혀 나타나지 않을 수 있다. 부트스트랩 샘플을 사용함으로써 bagging은 모델 간에 다양성을 창출하고 최종 예측의 분산을 줄일 수 있다.

bagging의 가장 일반적인 응용 중 하나는 의사결정 트리(decision tree)를 기본 모델로 사용하는 것이다. 의사결정 트리는 데이터의 복잡한 패턴과 상호작용을 포착할 수 있는 강력하고 유연한 모델이다. 그러나 과적합되기 쉬운데, 이는 훈련 데이터에서는 잘 수행되지만 테스트 데이터에서는 잘 수행되지 않는다는 것을 의미한다. bagging을 사용하면 데이터의 노이즈와 이상치에 덜 민감하고 데이터 분포의 변화에 더 강건한 의사결정 트리의 앙상블을 만들 수 있다.

이 절에서는 scikit-learns Bagging Classifier 클래스를 사용하여 bagging을 구현하는 방법을 보일 것이다. 또한 실제 분류 문제에 적용하는 방법, 즉 의학적 속성에 따라 사람의 심장 질환 여부를 예측하는 방법도 배울 것이다. 사용할 데이터 세트는 심장 질환 UCI 데이터 세트로, 13개의 속성을 가진 환자의 303개의 기록과 이진 표적 변수를 포함한다.

bagging을 구현하려면 다음 단계를 따라야 한다.

1. 데이터 로드와 탐색
1. 데이터를 훈련과 테스트 세트로 분할
1. bagging 분류기 생성괴 핏(fit)
1. 예측과 성능 평가

먼저 데이터를 로드하고 탐색하는 첫 번째 단계부터 시작하겠다.

```python
# Load the data
df = pd.read_csv("heart.csv")
# Explore the data
df.head()
df.info()
df.describe()
df['target'].value_counts()
```

코드의 출력은 아래와 같다.

```
# Output of df.head()
   age  sex  cp  trestbps  chol  fbs  restecg  thalach  exang  oldpeak  slope  ca  thal  target
0   63    1   3       145   233    1        0      150      0      2.3      0   0     1       1
1   37    1   2       130   250    0        1      187      0      3.5      0   0     2       1
2   41    0   1       130   204    0        0      172      0      1.4      2   0     2       1
3   56    1   1       120   236    0        1      178      0      0.8      2   0     2       1
4   57    0   0       120   354    0        1      163      1      0.6      2   0     2       1

# Output of df.info()
RangeIndex: 303 entries, 0 to 302
Data columns (total 14 columns):
 #   Column    Non-Null Count  Dtype  
---  ------    --------------  -----  
 0   age       303 non-null    int64  
 1   sex       303 non-null    int64  
 2   cp        303 non-null    int64  
 3   trestbps  303 non-null    int64  
 4   chol      303 non-null    int64  
 5   fbs       303 non-null    int64  
 6   restecg   303 non-null    int64  
 7   thalach   303 non-null    int64  
 8   exang     303 non-null    int64  
 9   oldpeak   303 non-null    float64
 10  slope     303 non-null    int64  
 11  ca        303 non-null    int64  
 12  thal      303 non-null    int64  
 13  target    303 non-null    int64  
dtypes: float64(1), int64(13)
memory usage: 33.3 KB
# Output of df.describe()
          age         sex          cp    trestbps        chol         fbs     restecg     thalach       exang     oldpeak       slope          ca        thal      target
count  303.000000  303.000000  303.000000  303.000000  303.000000  303.000000  303.000
```

## <a nmae="sec_04"></a> Boosting: 적응형 boosting과 그래디언트 boosting
boosting은 또 다른 대중적이고 강력한 앙상블 학습 방법이다. 이는 여러 모델을 수정된 데이터 버전에 따라 순차적으로 학습시킨 다음, 그들의 성능에 따라 예측값에 가중치를 부여하는 것을 포함한다. 부스팅은 모델의 편향과 분산을 줄이고 정확도를 향상시킬 수 있다.

boosting 알고리즘에는 다양한 종류가 있지만, 이 절에서는 가장 일반적인 것 중 두 방법을 설명할 것이다. 가장 처음이고 간단한 부스팅 알고리즘 중 하나는 적응형 boosting(adaptive boosting), 즉 `AdaBoost`이다. 각 반복에서 잘못 분류된 샘플에 더 높은 가중치를 부여하여 후속 모델이 어려운 경우에 더 집중할 수 있도록 함으로써 작동한다. 그래디언트 boosting(gradient boosting), 즉 `GBM`은 더 발전되고 유연한 boosting 알고리즘이다. 이전 모델의 잔차(residual)에 후속 모델을 맞추어 앙상블이 손실 함수를 최소화할 수 있도록 함으로써 작동한다.

이 절에서는 scikit-learns의 `AdaBoostClassifier`와 `GradientBoostClassifier` 클래스를 사용하여 적응형 boosting과 그라디언트 boosting을 구현하는 방법을 설명한다. 또한 이전과 동일한 분류 문제, 즉 의학적 속성에 따라 사람의 심장 질환 여부를 예측하는 문제에 적용하는 방법도 보일 것이다. 사용할 데이터세트는 이전과 동일하다. 심장 질환 UCI 데이터 세트는 13개 피처을 가진 환자의 303개 기록과 이진 목표 변수를 포함한다.

적응형 boosting과 그래디언트 boosting을 구현하려면 다음 단계를 따라야 한다.

1. 데이터 로드와 분할(이전과 동일)
1. 적응형 boosting 분류기 생성과 적합
1. 그래디언트 boosting 분류기 생성과 적합
1. 예측과 성능 평가
1. 어댑티브 boosting과 그래디언트 boosting의 결과 비교

먼저 데이터를 로드하고 분할하는 첫 번째 단계부터 시작한다. 이는 이전과 동일하므로 동일한 코드를 재사용할 수 있다.

```python
# Load the data
df = pd.read_csv("heart.csv")
# Split the data into features and target
X = df.drop('target', axis=1)
y = df['target']
# Split the data into training and test sets
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)
```

이제 두 번째 단계인 적응형 boostig 분류기 생성과 피팅으로 넘어가자.

다음 코드를 사용하여 `AdaBoostClassifier`를 만들고 피트한다.

```python
from sklearn.ensemble import AdaBoostClassifier
from sklearn.metrics import accuracy_score


# Create AdaBoost classifier
ada_clf = AdaBoostClassifier(n_estimators=50, random_state=42)

# Fit the classifier to the training data
ada_clf.fit(X_train, y_train)

# Make predictions on the test data
y_pred_ada = ada_clf.predict(X_test)

# Evaluate the performance of the classifier
accuracy_ada = accuracy_score(y_test, y_pred_ada)
print("Accuracy of AdaBoost Classifier:", accuracy_ada)
```

이 코드는 50개의 기본 추정기로 `AdaBoostClassifier`를 만들고 이를 훈련 데이터에 맞춘다. 그런 다음 테스트 데이터에 대한 예측을 수행하고 분류기의 정확도를 평가한다.

다음은 그래디언트 boosting 분류기를 만들고 맞추는 것으로 넘어간다.

`GradientBoostingClassifier`를 만들고 피트하려면 다음 코드를 사용한다.

```python
from sklearn.ensemble import GradientBoostingClassifier


# Create Gradient Boosting classifier
gb_clf = GradientBoostingClassifier(n_estimators=100, learning_rate=0.1, random_state=42)

# Fit the classifier to the training data
gb_clf.fit(X_train, y_train)

# Make predictions on the test data
y_pred_gb = gb_clf.predict(X_test)

# Evaluate the performance of the classifier
accuracy_gb = accuracy_score(y_test, y_pred_gb)
print("Accuracy of Gradient Boosting Classifier:", accuracy_gb)
```

이 코드는 100명의 기본 학습자와 0.1의 학습률로 `GradientBoostingClassifier`를 생성하고 이를 훈련 데이터에 피트한다. 그런 다음 테스트 데이터에 대한 예측을 수행하고 분류기의 정확도를 평가한다.

마지막으로 AdaBoost와 Gradient Boosting 분류기의 결과를 비교할 수 있다.

```python
print("Accuracy of AdaBoost Classifier:", accuracy_ada)
print("Accuracy of Gradient Boosting Classifier:", accuracy_gb)
```

이렇게 하면 테스트 데이터에 대한 두 분류기의 정확도를 얻을 수 있으므로 성능을 비교할 수 있다.

## <a nmae="sec_05"></a> Stacking: 적층 일반화
stacking은 또 다른 진보적이고 강력한 앙상블 학습 방법이다. 동일하거나 다른 데이터에 대해 여러 모델을 훈련시킨 다음 다른 모델을 사용한 예측을 결합하는 것이다. stacking은 다른 모델의 장점을 활용하고 다양성을 향상시킬 수 있다.

stacking을 구현하는 여러 가지 방법이 있지만 이 절에서는 간단하고 효과적인 접근법, 즉 로지스틱 회귀 모형을 메타 학습자(meta-learner)로 사용하는 방법을 설명한다. 메타 학습자는 기본 모덿의 예측값을 입력으로 받아 최종 예측값을 출력하는 모형이다. 로지스틱 회귀 분석은 범주형 입력과 연속형 입력을 모두 처리할 수 있고 확률 추정값을 출력할 수 있기 때문에 메타 학습자에게 좋은 선택이다.

이 절에서는 scikit-learns `StackingClassifier` 클래스를 사용하여 stacking 구현 방법을 설명할 것이다. 또한 이전과 동일한 분류 문제, 즉 의학적 속성에 따라 사람의 심장 질환 여부를 예측하는 문제에 적용하는 방법도 보일 것이다. 사용할 데이터 세트는 이전과 동일한데, 심장 질환 UCI 데이터 세트는 13개의 피처를 가진 환자의 303개의 기록과 이진 표적 변수를 포함한다.

stacking을 구현하려면 다음 단계를 수행하여야 한다.

1. 데이터 로드와 분할(이전과 동일)
1. stacking 분류기 생성과 적합
1. 예측과 성능 평가
1. bagging 및 boosting과 stacking 결과를 비교

먼저 데이터를 로드하고 분할하는 첫 번째 단계부터 시작하자. 이는 이전과 동일하므로 동일한 코드를 재사용할 수 있다.

```python
# Load the data
df = pd.read_csv("heart.csv")
# Split the data into features and target
X = df.drop('target', axis=1)
y = df['target']
# Split the data into training and test sets
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)
```

이제 두 번째 단계인 stacking 분류기 생성과 피팅으로 넘어가겠다.

stacking 분류기를 만들고 피팅하려면 다음 코드를 사용한다.

```python
from sklearn.ensemble import StackingClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import AdaBoostClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score


# Define base estimators
base_estimators = [
    ('rf', RandomForestClassifier(n_estimators=50, random_state=42)),
    ('ada', AdaBoostClassifier(n_estimators=50, random_state=42)),
    ('gb', GradientBoostingClassifier(n_estimators=100, learning_rate=0.1, random_state=42))
]

# Create stacking classifier
stack_clf = StackingClassifier(estimators=base_estimators, final_estimator=LogisticRegression())

# Fit the classifier to the training data
stack_clf.fit(X_train, y_train)

# Make predictions on the test data
y_pred_stack = stack_clf.predict(X_test)

# Evaluate the performance of the classifier
accuracy_stack = accuracy_score(y_test, y_pred_stack)
print("Accuracy of Stacking Classifier:", accuracy_stack)
```

이 코드는 Random Forest, AdaBoost, Gradient Boosting을 기본 추정량으로 하고 Logistic Regression을 최종 추정량으로 하는 stacking 분류기를 생성한다. 훈련 데이터에 분류기를 피트하고 테스트 데이터에 대한 예측을 하며 분류기의 정확도를 평가한다.

마지막으로 bagging 및 boosting과 적층한 결과를 비교한다.

```python
print("Accuracy of Bagging Classifier:", accuracy_bag)
print("Accuracy of AdaBoost Classifier:", accuracy_ada)
print("Accuracy of Gradient Boosting Classifier:", accuracy_gb)
print("Accuracy of Stacking Classifier:", accuracy_stack)
```

이렇게 하면 테스트 데이터에 대한 네 가지 분류기(Bagging, AdaBoost, Gradient Boost, Stacking)의 정확도를 모두 얻을 수 있으므로 성능을 비교할 수 있다.

## <a nmae="sec_06"></a> 앙상블 방법의 비교와 평가
이 절에서는 심장병 분류 문제에 대해 서로 다른 앙상블 방법들의 성능을 비교 평가gks다. 또한 결과를 해석하는 방법과 몇 가지 결론을 도출하는 방법도 배우게 된다.

다양한 앙상블 방법의 성능을 비교와 평가하려면 다음 단계를 수행하여야 한다.

1. 테스트세트에서 각 앙상블 방법의 정확도 점수를 계산한다
1. 각 앙상블 방법의 혼동 행렬을 검정 세트에 표시한다
1. 테스트세트에 대한 각 앙상블 방법의 분류 보고서를 생성한다
1. 결과 분석과 결론 도출

첫 번째 단계인 테스트세트에서 각 앙상블 방법의 정확도 점수를 계산하는 것부터 시작한다. 정확도 점수(accuracy score)는 전체 예측 수 중에서 몇 개의 예측이 정확한지를 측정하는 간단한 메트릭입니다. scikit-learn의 `accuracy_score` 함수를 사용하여 각 앙상블 방법의 정확도 점수를 계산할 수 있다. 실제 레이블(`y_test`)과 예측 레이블(`y_pred`)을 함수에 인수로 전달해야 한다. 나중에 비교를 위해 정확도 점수를 사전에 저장할 수 있다. 작업을 수행할 수 있는 코드는 다음과 같다.

```python
# Import the accuracy_score function
from sklearn.metrics import accuracy_score


# Create a dictionary to store the accuracy scores
accuracy_scores = {}

# Calculate the accuracy score of bagging
y_pred_bag = bag_clf.predict(X_test)
accuracy_scores['bagging'] = accuracy_score(y_test, y_pred_bag)

# Calculate the accuracy score of adaptive boosting
y_pred_ada = ada_clf.predict(X_test)
accuracy_scores['adaptive boosting'] = accuracy_score(y_test, y_pred_ada)

# Calculate the accuracy score of gradient boosting
y_pred_gbm = gbm_clf.predict(X_test)
accuracy_scores['gradient boosting'] = accuracy_score(y_test, y_pred_gbm)

# Calculate the accuracy score of stacking
y_pred_stack = stack_clf.predict(X_test)
accuracy_scores['stacking'] = accuracy_score(y_test, y_pred_stack)

# Print the accuracy scores
print(accuracy_scores)
```

코드의 출력은 아래와 같다.

```
# Output of accuracy_scores
{'bagging': 0.8524590163934426, 'adaptive boosting': 0.8852459016393442, 'gradient boosting': 0.8688524590163934, 'stacking': 0.8688524590163934}
```

보다시피 앙상블 방법의 정확도 점수는 85%에서 89%로 상당히 높다. 이는 앙상블 방법이 테스트세트에서 대부분의 경우를 정확하게 예측할 수 있다는 것을 의미한다. 네 가지 앙상블 방법 중 적응형 boosting이 정확도 점수가 가장 높고, 그다음으로 정확도 점수가 동일한 그래디언트 boosting과 stacking, 그리고 정확도 점수가 가장 낮은 bagging이다. 이는 적응형 boosting이 이 문제에 대해 가장 효과적인 앙상블 방법인 반면 bagging이 가장 효과적이지 않다는 것을 시사한다.

그러나 정확도 점수는 앙상블 방법의 성능을 평가하는 데 사용해야 하는 유일한 지표는 아니다. 데이터가 불균형하다면 정확도 점수는 오해의 소지가 있을 수 있으며, 이는 한 클래스가 다른 클래스보다 더 자주 발생한다는 것을 의미한다. 그 경우 항상 다수 클래스를 예측하는 모델은 정확도 점수가 높을 수 있지만 좋은 모델은 아니다. 따라서 앙상블 방법의 성능을 보다 포괄적으로 파악하려면 혼동 행렬과 분류 보고서와 같은 다른 메트릭도 사용해야 한다.

두 번째 단계인 검정 세트에 각 앙상블 방법의 혼동 행렬을 표시하는 것으로 넘어가자. 혼동 행렬은 참 금정(true positive), 거짓 긍정(false positive), 참 부정(true negative) 및 거짓 부정(false negative)이 각각 몇 개의 예측인지를 보여주는 표이다. 참 긍정은 모델이 긍정의 클래스를 정확하게 예측한다는 것을 의미하고, 거짓 긍정는 모델이 긍정의 클래스를 잘못 예측한다는 것을 의미하며, 참 부정는 모델이 부정의 클래스를 정확하게 예측한다는 것을 의미하고, 거짓 부정은 모델이 부정의 클래스를 잘못 예측한다는 것을 의미한다. scikit-learn의 compusion_matrix 함수를 사용하여 각 앙상블 방법의 혼동 행렬을 계산할 수 있다. Seaborn의 히트맵 함수를 사용하여 혼동 행렬을 히트맵으로 표시할 수도 있다. 이를 위한 코드는 다음과 같다.

```python
# Import the confusion_matrix function
from sklearn.metrics import confusion_matrix
# Import the heatmap function
from seaborn import heatmap


# Create a list of ensemble methods
ensemble_methods = ['bagging', 'adaptive boosting', 'gradient boosting', 'stacking']
# Create a list of predicted labels
predicted_labels = [y_pred_bag, y_pred_ada, y_pred_gbm, y_pred_stack]
# Loop through the ensemble methods and predicted labels
for method, label in zip(ensemble_methods, predicted_labels):
    # Calculate the confusion matrix
    cm = confusion_matrix(y_test, label)
    # Plot the confusion matrix as a heatmap
    plt.figure(figsize=(6, 6))
    heatmap(cm, annot=True, fmt='d', cmap='Blues')
    plt.title(f'Confusion Matrix of {method}')
    plt.xlabel('Predicted')
    plt.ylabel('Actual')
    plt.show()
```

## <a nmae="summary"></a> 마치며
이 포스팅에서는 Python에서 bagging, 적응형 boosting, 그래디언트 boosting, stacking 네 가지 앙상블 학습 방법을 배웠다. 의학적 속성에 따라 심장병 여부를 예측하는 실세계 분류 문제에 적용하여 보았다. 정확도 점수, 혼동 행렬, 분류 보고서 등 다양한 지표를 사용하여 다양한 앙상블 방법의 성능을 비교하고 평가하였다.

다음은 이 포스팅의 주요 내용이다.

- 앙상블 학습은 기계 학습 모델의 성능과 정확성을 향상시키기 위한 강력한 기술이다. 데이터의 서로 다른 부분 집합 또는 변형에 대해 각각 훈련된 여러 모델을 결합하여 어떤 단일 모델보다 강력하고 신뢰할 수 있는 최종 예측을 생성하는 것을 포함한다.
- 모델을 어떻게 훈련하고 어떻게 조합하느냐에 따라 앙상블 학습 방법의 타입이 달라진다. 이 포스팅에서는 크게 세 가지 타입의 앙상블 학습 방법인 ㅠㅁㅎ햐ㅜㅎbagging, boosting, stacking을 사용하는 방법을 배웠다.
- bagging은 부트스트랩 집계(bootstrap aggregation)의 약자이다. 이는 여러 모델들을 각각 데이터의 다른 임의의 부분집합에 대해 훈련시킨 다음 예측의 평균을 낸다. bagging은 모델의 분산을 줄이고 모델의 일반화 능력을 향상시킬 수 있다.
-boosting은 여러 모델을 수정된 버전의 데이터에 대해 순차적으로 훈련시킨 다음, 그들의 성능에 따라 예측값에 가중치를 부여하는 것이다. boosting은 모델의 편향과 분산을 줄이고 정확도를 향상시킬 수 있다.
- stacking은 동일하거나 다른 데이터에 대해 여러 모델을 훈련시킨 다음 다른 모델을 사용하여 예측을 결합하는 것이다. stacking은 다양한 모델의 장점을 활용하여 다양성을 향상시킬 수 있다.
- 앙상블 학습 방법을 Python에서 구현하기 위해서는 앙상블 모델을 만들고 피팅하기 위한 다양한 클래스와 기능을 제공하는 scikit-learns 앙상블 모듈을 사용할 수 있다. 이 포스팅에서는 BaggingClassifier, AdaBoostClassifier, GradientBoostClassifier, StackingClassifier 클래스를 사용했다.
- 서로 다른 앙상블 방법의 성능을 비교 평가하기 위해 정확도 점수, 혼동 행렬, 분류 보고서 등 다양한 메트릭을 사용할 수 있다. 이러한 메트릭을 계산하고 표시하는 다양한 기능을 제공하는 scikit-learn의 메트릭 모듈도 사용할 수 있다. 이 포스팅에서는 accuracy_score, confusion_matric, classification_report 함수를 사용했다.
- 이 포스팅의 결과를 바탕으로 앙상블 방법이 테스트세트에서 85%에서 89%의 높은 정확도 점수를 달성할 수 있음을 알 수 있었다. 네 가지 앙상블 방법 중 적응형 부스팅이 가장 높은 정확도 점수를 가지고 있으며, 그 다음으로 동일한 정확도 점수를 가진 그래디언트 boosting과 stacking, 그리고 bagging이 가장 낮은 정확도 점수를 보였다. 이는 적응형 boosting이 이 문제에 대해 가장 효과적인 앙상블 방법인 반면 bagging이 가장 효과적이지 않음을 시사한다.
