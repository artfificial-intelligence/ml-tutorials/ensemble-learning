# 앙상블 학습: Bagging, Boosting과 Stacking <sup>[1](#footnote_1)</sup>

> <font size="3">모델 성능 향상을 위한 배깅, 부스팅, 스태킹 등 앙상블 학습법 활용법에 대해 알아본다.</font>

## 목차

1. [개요](./ensemble-learning.md#intro)
1. [앙상블 러닝이란?](./ensemble-learning.md#sec_02)
1. [Bagging: 부트스트랩 집계](./ensemble-learning.md#sec_03)
1. [Boosting: 적응형 boosting과 그래디언트 boosting](./ensemble-learning.md#sec_04)
1. [Stacking: 적층 일반화](./ensemble-learning.md#sec_05)
1. [앙상블 방법의 비교와 평가](./ensemble-learning.md#sec_06)
1. [마치며](./ensemble-learning.md#summary)

<a name="footnote_1">1</a>: [ML Tutorial 19 — Ensemble Learning: Bagging, Boosting, Stacking](https://ai.plainenglish.io/ml-tutorial-19-ensemble-learning-bagging-boosting-stacking-5a926db20ec5?sk=50fdff4c8be302f8c3b6d2e9915b563c)를 편역하였습니다.
